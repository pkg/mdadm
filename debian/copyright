Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: mdadm
Upstream-Contact: Jes Sorensen <jes@trained-monkey.org>
Source: https://www.kernel.org/pub/linux/utils/raid/mdadm/

Files: *
Copyright: 2001-2006 Neil Brown <neilb@suse.de>
License: GPL-2+

Files: debian/*
Copyright: 2023-2025 Daniel Baumann <daniel@debian.org>
           2020 Felix Lechner <felix.lechner@lease-up.com>
           2005-2008 Martin F. Krafft <madduck@debian.org>
           2001-2005 Mario Jou/3en <joussen@debian.org>
License: GPL-2+

License: GPL-2+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
 .
 The complete text of the GNU General Public License
 can be found in /usr/share/common-licenses/GPL-2 file.
